package negocio;

public class Tramo  implements Comparable<Tramo>  {
	private Punto _p1;
	private Punto _p2;
	private Double _peso;
	
	public Tramo(Punto p1, Punto p2, Double peso) {
		_p1=p1;
		_p2=p2;
		_peso=peso;
	}
	
	public boolean equals(Tramo otro) {
		return _p1.getId() == otro.getP1().getId() && _p2.getId()== otro.getP2().getId();
	}

	@Override
	public int compareTo(Tramo otro) {
		if(_peso<otro.getPeso()) 
			return 1;
		else if (_peso>otro.getPeso())
			return -1;
		return 0;	
	}
	
	public Double getPeso() {
		return _peso;
	}
	
	public Punto getP1() {
		return _p1;
	}
	

	public Punto getP2() {
		return _p2;
	}
	
	@Override
	public String toString() {
		return "Punto 1:" + _p1.toString() + ". Puunto 2:" + _p2.toString() + ". Peso: "+ _peso.toString();
	}
	
}
