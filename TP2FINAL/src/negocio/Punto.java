package negocio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Punto implements Serializable
{
	private String _latitud;
	private String _longitud;
	private ArrayList<Punto> _vecinos;
	private int _id;
	private double _latitudNumero;
	private double _longitudNumero;
	
	public Punto( String lat, String lon, int id){
		_latitud = lat;
		_longitud = lon;
		_vecinos = new ArrayList<Punto>();
		_id= id;
		_latitudNumero= Double.valueOf(_latitud);
		_longitudNumero= Double.valueOf(_longitud);
	}
	
	public int cantidadDeVecinos(){
		return _vecinos.size();
	}

	public boolean tieneVecino(Punto vecino) {
		return _vecinos.contains(vecino);
	}

	public void agregarVecino(Punto vecino) {
		try {
			_vecinos.add(vecino);
		} catch (Exception e) {
			
		}
	}
	
	public void eliminarVecino(Punto vecino) {
		_vecinos.remove(vecino);
	}

	
		public Punto getVecino(int index){
		return _vecinos.get(index);
	}
	
	@Override
	public String toString() {
		return "Latitud: "+ this._latitud + ". Longitud: "+this._longitud;
	}
	
	public static double distanciaEntreDosPuntos(Punto p1, Punto p2) {  
        double radioTierra = 6371;//en kilómetros  
        double dLat = Math.toRadians(p2.get_latitudNumero() - p1.get_latitudNumero());  
        double dLng = Math.toRadians(p2.get_longitudNumero()- p1.get_longitudNumero());  
        double sindLat = Math.sin(dLat / 2);  
        double sindLng = Math.sin(dLng / 2);  
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)* Math.cos(Math.toRadians(p1.get_latitudNumero())) * Math.cos(Math.toRadians(p2.get_latitudNumero()));  
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));  
        double distancia = radioTierra * va2;  
        
        return distancia;  
    }  
	
	public boolean esIgual(Punto p2) {
		return(getId()==p2.getId());
	}
	
	public int getId() {
		return _id;
	}

	public double distanciaConOtroPunto( Punto otroPunto) {  
        double radioTierra = 6371;//en kilómetros  
        double dLat = Math.toRadians(otroPunto.get_latitudNumero() - get_latitudNumero());  
        double dLng = Math.toRadians(otroPunto.get_longitudNumero()- get_longitudNumero());  
        double sindLat = Math.sin(dLat / 2);  
        double sindLng = Math.sin(dLng / 2);  
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)  
                * Math.cos(Math.toRadians(get_latitudNumero())) * Math.cos(Math.toRadians(otroPunto.get_latitudNumero()));  
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));  
        double distancia = radioTierra * va2;  
   
        return distancia;  
    }
	
	public ArrayList <Punto> getVecinos(){
		return _vecinos;
	}
	
	public double get_latitudNumero() {
		return _latitudNumero;
	}

	public void set_latitudNumero(double _latitudNumero) {
		this._latitudNumero = _latitudNumero;
	}

	public double get_longitudNumero() {
		return _longitudNumero;
	}

	public void set_longitudNumero(double _longitudNumero) {
		this._longitudNumero = _longitudNumero;
	}
	public String get_latitud() {
		return _latitud;
	}

	public void set_latitud(String _latitud) {
		this._latitud = _latitud;
	}

	public String get_longitud() {
		return _longitud;
	}

	public void set_longitud(String _longitud) {
		this._longitud = _longitud;
	}
	

	
}
