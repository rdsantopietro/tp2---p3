package negocio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GrafoPesado implements Serializable {

	private static final long serialVersionUID = 1L;

	private Grafo _grafo;

	public Double[][] pesos;

	private  Punto [] puntos;
	
	private int cantidadTramos;
	
	private List <Tramo> tramos;
	
	private Double pesoTotal;
	
	// Se construye un grafo pesado con una cantidad fija de vértices
	public GrafoPesado(int n) {
		_grafo = new Grafo(n);
		pesos = new Double[n][n];
		puntos= new Punto [n];
		cantidadTramos=0;
		pesoTotal=0.0;
		tramos=new ArrayList<Tramo>();
	}

	public void agregarPunto(Punto nuevoPunto) {
		verificarPunto(nuevoPunto);
		puntos[nuevoPunto.getId()]=nuevoPunto;
	}

	public void setPesoPorId(int i,int j, Double peso) {
		pesos[i][j]=peso;
		pesos[j][i]=peso;
	}
	
	public void agregarTramo(Punto p1, Punto p2, Double peso) {
		verificarPeso(peso);
		verificarPuntos(p1, p2);
		pesoTotal+=peso;
		setPesoPorId(p1.getId(), p2.getId(), peso);
		p1.agregarVecino(p2);
		p2.agregarVecino(p1);
		_grafo.agregarArista(p1.getId(), p2.getId());
		cantidadTramos++;
		tramos.add(new Tramo(p1, p2, peso));
	}
	
	public void quitarTramo(Punto p1,Punto  p2) {
		verificarPuntos(p1, p2);
		pesoTotal-= getPesoTramo(p1, p2);
		setPesoPorId(p1.getId(), p2.getId(), null);
		p1.eliminarVecino(p2);
		p2.eliminarVecino(p1);
		_grafo.eliminarArista(p1.getId(), p2.getId());
		Tramo t = new Tramo (p1,p2 , 0.0);
		tramos.remove(t);
		cantidadTramos--;
	}
	
	//VERIFICACIONES
	private void verificarPuntos(Punto p1, Punto p2) {
			verificarPunto(p1);
			verificarPunto(p2);
			if (p1.getId() == p2.getId())
				throw new IllegalArgumentException("Mismo Punto " + p1.getId());
	}
	
	private void verificarPunto(Punto p1) {
		if (p1.getId() < 0 || p1.getId()  >= this.puntos.length)
			throw new IllegalArgumentException("El Id " + p1.getId() + " no existe");
	}
	
	private void verificarPeso(Double peso) {
		if (peso< 0.0)
			throw new IllegalArgumentException("El peso no puede ser negativo :"+ String.valueOf(peso) );
	}
	//VERIFICACIONES

	public boolean existeTramo(Punto p1, Punto p2) {
		verificarPuntos(p1, p2);
		return this.getGrafo().existeArista(p1.getId(), p2.getId());
	}
	
	public Punto getPunto(int i) {
		return puntos[i];
	}
	public int getCantidadTramos() {
		return cantidadTramos;
	}
	public Grafo getGrafo() {
		return _grafo;
	}
	public Punto [] getPuntos() {
		return puntos;
	}
	
	public boolean existeArista(int i, int j) {
		return _grafo.existeArista(i, j);
	}
	public Double getPesoTramo(Punto p1 , Punto p2) {
		return pesos[p1.getId()][p2.getId()];
	}
	
	public int tamano() {
		return puntos.length;
	}
	
	public List <Tramo> getTramos(){
		return tramos;
	}

	public void eliminoTramoMasGrande() {
		quitarTramo(tramos.get(0).getP1(), tramos.get(0).getP2());
		tramos.remove(0);
	}
	public void ordenoTramos() {
		Collections.sort(tramos);
	}
	public Double pesoTotal() {
		return pesoTotal;
	}

}
