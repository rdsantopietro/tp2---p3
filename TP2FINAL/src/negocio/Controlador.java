package negocio;
import datos.*;
import entornoGrafico.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Controlador {

	public DataManager dataManager;
	private InstanciaProblema instancia; // GRAFO CREADO CON LOS VALORES DE LA ISNTANCIA
	private Ventana ventana;
	private ArrayList<String> instanciaTXT;//INSTANCIA LEIDA DEL TXT
	private DataManager datos;
	private String instanciaElegida;
	private Dibujador dibujador;
	private Resultados resultados;
	

	public Controlador() {
		instancia = new InstanciaProblema();
		instanciaElegida = " ";
		datos = new DataManager();
		ventana = new Ventana();
		ventana.llenoComboInstancias(datos.instanciasDisponibles());
		this.ventana.escuchoProcesar(new cargaProcesarListener());
		this.ventana.escuchoDividir(new cargaDividirListener());
		dibujador = new Dibujador();
		resultados= new Resultados();
		
	}
	
	public static void main(String[] args) {
		
		Controlador controlador = new Controlador();
	}

	// ESCUCHA BOTON PROCESAR
	class cargaProcesarListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				resultados.cerrar();
				ventana.actualizoVentana();
				instanciaElegida = ventana.getInstanciaElegida();
				cargoInstanciaLeida();
				instancia.creoGrafoPesadoDeInstancia(tamanoInstanciaTXT());
				instancia.cargoPuntosEnGrafo(instanciaTXT);
				instancia.calculoDistanciasInstancia();
				instancia.ejecutoPrim();
				ventana.reinicioMapa();
				ventana.centrarMapa(instancia.latitudInicial(), instancia.longitudInicial());
				dibujador.DibujoGrafoPesadoConTramos(instancia.getGrafoSolucion(), ventana);
				instancia.getGrafoSolucion().ordenoTramos();
				ventana.activarPanelDividir();
			} catch (NumberFormatException ex) {
			
			} 

		}
	}
	
	// ESCUCHA BOTON QUITAR TRAMOS
	class cargaDividirListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				
				ventana.reinicioMapa();
				instancia.quitarTramosPesados(ventana.getCantidadDePartes());
				ventana.actualizoVentana();
				dibujador.DibujoGrafoPesadoConTramos(instancia.getGrafoSolucion(), ventana);
				ventana.actualizoVentana();
				ventana.desactivaPanelDividir();
				String [] columnas= {"GRAFO","GRAFO","GRAFO","GRAFO" }; 
				String [][] matriz = datos.creoMatriz(instancia);
				resultados.llenoTabla(matriz, columnas  );
				resultados.mostrar();
				} catch (NumberFormatException ex) {
			} 

		}
	}

	
	public void llenoComboInstancias() {
		ventana.llenoComboInstancias(datos.instanciasDisponibles());
	}
	
	private void cargoInstanciaLeida() {
		instanciaTXT = new ArrayList<String>();
		instanciaTXT = datos.leerArchivoInstancia(instanciaElegida);
	}
	
	public int tamanoInstanciaTXT() {
		return instanciaTXT.size();
	}
	

}
