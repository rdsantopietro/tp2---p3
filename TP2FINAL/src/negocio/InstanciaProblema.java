package negocio;

import java.util.ArrayList;

public class InstanciaProblema {
	
	private GrafoPesado grafo;
	private GrafoPesado grafoSolucion;
	
	public void creoGrafoPesadoDeInstancia(int i) {
		grafo = new GrafoPesado(i);
		grafoSolucion = new GrafoPesado(i);
	}
	
	public void cargoPuntosEnGrafo(ArrayList<String> instancia) {
		for (int x = 0; x < grafo.tamano() ; x++) {
			String[] datos = instancia.get(x).split(" ");
			Punto p = new Punto(datos[0], datos[1], x);
			try {
				grafo.agregarPunto(p);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void calculoDistanciasInstancia() {
		for (Punto punto : grafo.getPuntos()) {
			for (Punto punto2: grafo.getPuntos()) {
				if(!punto.esIgual(punto2)) {
					grafo.agregarTramo(punto, punto2, punto.distanciaConOtroPunto(punto2));
				}
			}
		}
	}
	
	public void ejecutoPrim() {
		Prim prim= new Prim(grafo);
		grafoSolucion=prim.ejecutar();
	}
	
	public GrafoPesado getGrafoSolucion() {
		return grafoSolucion;
	}
	
	public Punto getPunto(int i ) {
		return grafo.getPunto(i);
	}
	
	public GrafoPesado getGrafo() {
		return grafo;
	}

	public int tamano() {
		return grafo.tamano();
	}

	public void quitarTramosPesados(String cantidadDePartes) {
		int cantidad = Integer.valueOf(cantidadDePartes); 
		for (int i = 0; i < cantidad; i++) {
			getGrafoSolucion().eliminoTramoMasGrande();
		}
	}
	
	public int cantidadPuntos() {
		return grafo.tamano();
	}
	
	public Double latitudInicial() {
		return grafo.getPunto(0).get_latitudNumero();
	}
	public Double longitudInicial() {
		return grafo.getPunto(0).get_longitudNumero();
	}
	
}
