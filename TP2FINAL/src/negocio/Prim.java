package negocio;
import java.util.ArrayList;

public class Prim {

	private GrafoPesado grafo;
	private int cantNodos;
	private int cantAristas;
	private GrafoPesado grafoSolucion;
	public Double[][]  solucion;
	private int[] padres;
	private ArrayList<Integer> nodosConectados;
	private ArrayList<Integer> nodosNoConectados;
	private Double minimoCosto;
	
	public GrafoPesado getGrafo() {
		return grafo;
	}


	public Prim(GrafoPesado grafo) {
		this.grafo = grafo;
		System.out.println(this.grafo.tamano());
		cantNodos = grafo.tamano();
		cantAristas = grafo.getCantidadTramos();
		grafoSolucion = new GrafoPesado(cantNodos);
		new ArrayList<Tramo>();
		solucion = new Double[cantNodos][cantNodos]; 
		padres = new int[cantNodos];
		inicializarPadres();
		minimoCosto = 0.0;
		nodosNoConectados = new ArrayList<Integer>();
		nodosConectados = new ArrayList<Integer>();
	}

		public GrafoPesado ejecutar()  {
			for (int i = 0; i < cantNodos; i++) {
				nodosNoConectados.add(i);
				grafoSolucion.agregarPunto(new Punto(grafo.getPunto(i).get_latitud(), grafo.getPunto(i).get_longitud(),i));
			}

		int nodoInicial = (int) (Math.random() * cantNodos);
		nodosNoConectados.remove(nodosNoConectados.indexOf(nodoInicial));
		nodosConectados.add(nodoInicial);
		int nodoCreceRama = 0;
		int nodoMasCercano = 0;
		// mientras el árbol no conecte todas los nodos
		while (nodosConectados.size() < cantNodos ) {
			Double distancia = 100.0;
			for (int nodoActual : nodosConectados) {
				for (int nodo : nodosNoConectados) {
					if (grafo.getGrafo().existeArista(nodoActual, nodo)) {
						if (distancia == 100.0 || grafo.getPesoTramo(grafo.getPunto(nodoActual), grafo.getPunto(nodo)) < distancia) {
							if (find(nodoActual) != find(nodo)) {
								distancia = grafo.getPesoTramo(grafo.getPunto(nodoActual), grafo.getPunto(nodo));
								nodoMasCercano = nodo;
								nodoCreceRama = nodoActual;
							}
						}
					}
				}
			}
			
			union(nodoCreceRama, nodoMasCercano);
			if (nodoCreceRama < nodoMasCercano) {
				solucion[nodoCreceRama ] [nodoMasCercano] = distancia;
			} else {
				solucion [nodoMasCercano] [nodoCreceRama ] = distancia;
			}
			grafoSolucion.agregarTramo(grafoSolucion.getPunto(nodoCreceRama), grafoSolucion.getPunto(nodoMasCercano), distancia);
			minimoCosto += distancia;
			nodosNoConectados.remove(nodosNoConectados.indexOf(nodoMasCercano));
			nodosConectados.add(nodoMasCercano);
		}

		return grafoSolucion;
	}

	public void inicializarPadres() {
		for (int i = 0; i < padres.length; i++) {
			padres[i] = i;
		}
	}

	public int find(int nodo) {
		return padres[nodo] == nodo ? nodo : find(padres[nodo]);
	}

	public void union(int nodo1, int nodo2) {
		padres[find(nodo1)] = padres[find(nodo2)];
	}

	public GrafoPesado getGrafoSolucion() {
		return grafoSolucion;
	}

	public void setGrafoSolucion(GrafoPesado grafo) {
		grafoSolucion = grafo;
	}
	public int getCantNodos() {
		return cantNodos;
	}

	public int getCantAristas() {
		return cantAristas;
	}

	public Double getMinimoCosto() {
		return minimoCosto;
	}



}