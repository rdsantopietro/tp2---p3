package negocio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Grafo implements Serializable {
	private boolean[][] _puntos;
	private static final long serialVersionUID = 1L;

	public Grafo(int n) {
		_puntos = new boolean[n][n];

	}

	public void agregarArista(int i, int j) {
		verificarIndices(i, j);	
		_puntos[i][j] = true;
		_puntos[j][i] = true;
	}

	public void eliminarArista(int i, int j) {
		verificarIndices(i, j);
		_puntos[i][j] = false;
		_puntos[j][i] = false;
	}

	public boolean existeArista(int i, int j) {
		verificarIndices(i, j);
		return _puntos[i][j] == true && _puntos[j][i] == true;
	}

	public Integer tamanioGrafo() {
		return _puntos.length;
	}

	public boolean existenVecinosDe(int j) {
		for (boolean bs : _puntos[j]) {
			if (bs == true)
				return true;
		}
		return false;
	}

	private void verificarIndices(int i, int j) {
		verificarVertice(i);
		verificarVertice(j);

		if (i == j)
			throw new IllegalArgumentException("Misma arista " + i);
	}

	private void verificarVertice(int i) {
		if (i < 0 || i >= tamanioGrafo())
			throw new IllegalArgumentException("El vertice " + i + " no existe");
	}

}
