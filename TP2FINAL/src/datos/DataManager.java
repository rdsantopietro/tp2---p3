package datos;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import negocio.InstanciaProblema;

public class DataManager {

	public ArrayList<String> leerArchivoInstancia(String archivo) {
		ArrayList <String> datos= new ArrayList<String>();
		String contenido = null;
		try {
			contenido = new String(Files.readAllBytes(Paths.get(archivo)));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String[] lineas = contenido.split("\\r?\\n");
		for (String line : lineas) 
			datos.add(line);
		
		return datos;
	}
	
	public ArrayList<String> instanciasDisponibles () {
		ArrayList<String> instancias = new ArrayList<String>();
		for (int i = 0; i < 5; i++) 
			instancias.add("instancia"+String.valueOf(i+1)+".txt");
		
		return instancias;
	}
	
	public String [][]  creoMatriz(InstanciaProblema instancia) {
		String [][] matriz = new String [3][4];
		matriz [0][0]="Grafo";
		matriz [0][1]="Puntos";
		matriz [0][2]="Tramos";
		matriz [0][3]="Distancia";
		matriz [1][0]="Grafo Inicial";
		matriz [1][1]=String.valueOf(instancia.cantidadPuntos());
		matriz [1][2]=String.valueOf(instancia.getGrafo().getCantidadTramos());
		matriz [1][3]=String.valueOf(instancia.getGrafo().pesoTotal().shortValue());
		matriz [2][0]="Grafo Solucion";
		matriz [2][1]=String.valueOf(instancia.cantidadPuntos());
		matriz [2][2]=String.valueOf(instancia.getGrafoSolucion().getCantidadTramos());
		matriz [2][3]=String.valueOf(instancia.getGrafoSolucion().pesoTotal().shortValue());
		return matriz;
	}
	
	

}