package entornoGrafico;

import java.util.ArrayList;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import negocio.GrafoPesado;
import negocio.InstanciaProblema;
import negocio.Punto;

public  class Dibujador {
	  public void dibujarInstancia(InstanciaProblema instancia, Ventana ventana ) {
		   DibujoGrafoPesado(instancia.getGrafo(), ventana);
	  }
	  
	  public void DibujoGrafoPesado(GrafoPesado grafo, Ventana ventana) {
		  for (int i = 0; i < grafo.tamano(); i++) {
		    	dibujarPunto(grafo.getPunto(i),ventana);
		    } 
	  }
	  
	  public void DibujoGrafoPesadoConTramos(GrafoPesado grafo, Ventana ventana) {
		  
		  for (int i = 0; i < grafo.tamano(); i++) {
			  dibujarPunto(grafo.getPunto(i),ventana);
			  for (Punto puntoVecino: grafo.getPunto(i).getVecinos()) {
				  dibujarLinea(grafo.getPunto(i), puntoVecino, ventana);
			  }
		  } 
	  }
	  
	  public void dibujarPunto(Punto e, Ventana ventana) {
		    Coordinate coord = new Coordinate(Double.parseDouble(e.get_latitud()), Double.parseDouble(e.get_longitud()));
		    MapMarkerDot n = new MapMarkerDot(coord);
		    ventana.get_mapa().addMapMarker(n);
	  }
	  
	  public void dibujarLinea(Punto p1, Punto p2, Ventana ventana){
		  ArrayList<Coordinate> c = new ArrayList<Coordinate>();
		  Coordinate coord = new Coordinate(Double.parseDouble(p1.get_latitud()), Double.parseDouble(p1.get_longitud()));
		  c.add(coord);
		  Coordinate coord2 = new Coordinate(Double.parseDouble(p2.get_latitud()), Double.parseDouble(p2.get_longitud()));
		  c.add(coord2);
		  ventana.get_mapa().addMapPolygon(new MapPolygonImpl(coord, coord2, coord));
	  }
	  
}
