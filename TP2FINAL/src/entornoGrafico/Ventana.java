package entornoGrafico;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.ICoordinate;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JComboBox;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class Ventana {

	private JFrame frame;
	private JPanel panelMapa;
	private JPanel panelControles;
	private JMapViewer _mapa;
	private ArrayList<Coordinate> _lasCoordenadas;
	private MapPolygonImpl _poligono;
	private JButton btnDibujarPolgono;
	private JComboBox comboInstancias;
	private String instanciaElegida;
	private JButton botonProcesar;
	private String cantidadDePartes;
	private JPanel panel;
	private JButton botonDividir;
	private JTextField textField;
	private UIManager.LookAndFeelInfo[] temas = UIManager.getInstalledLookAndFeels();
	
	 /**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana window = new Ventana();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

		/**
		 * Create the application.
		 */
		public Ventana() 
		{
			initialize();
		}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 725, 506);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		panelMapa = new JPanel();
		panelMapa.setBounds(10, 11, 437, 446);
		frame.getContentPane().add(panelMapa);

		panelControles = new JPanel();
		panelControles.setBounds(457, 11, 242, 446);
		frame.getContentPane().add(panelControles);
		inicializoMapa();
		frame.setVisible(true);
		instanciaElegida =" ";
		dibujarPoligono();
		eliminarPoligono();

	}
	public void cerrarVentana(){
		frame.dispose();
	}
	
	public void actualizoVentana() {
		SwingUtilities.updateComponentTreeUI(frame);
		
	}
	
	private void dibujarPoligono() {
		btnDibujarPolgono = new JButton("Dibujar Pol\u00EDgono");
		btnDibujarPolgono.setBounds(10, 11, 195, 23);
		btnDibujarPolgono.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				_poligono = new MapPolygonImpl(_lasCoordenadas);
				_mapa.addMapPolygon(_poligono);
			}
		});

	}
	
	//OBTIENE LOS NOMBRES DE L&F PARA MOSTRAR EN SELECT
	public String [] nombresLookAndFeel() {
		String [] nombres = new String [temas.length];
		for (int i = 0; i < temas.length; i++) {
			nombres[i]=temas[i].getName();
		}
			return nombres;
	}
	//OBTIENE LOS NOMBRES DE CLASE DE L&F 
	public void seleccionarLookAndFeel(String string) {
		String lookAndFeelClassName="";
		for (int i = 0; i < temas.length; i++) {
			if(string.equals(temas[i].getName())) {
				lookAndFeelClassName=temas[i].getClassName();
			}
		}
		try {
			UIManager.setLookAndFeel(lookAndFeelClassName);
			SwingUtilities.updateComponentTreeUI(frame);

        } catch(Exception e) {
            e.printStackTrace();
        }
	}

	private void eliminarPoligono() {
		GridBagLayout gbl_panelControles = new GridBagLayout();
		gbl_panelControles.columnWidths = new int[]{195, 0};
		gbl_panelControles.rowHeights = new int[]{23, 30, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panelControles.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panelControles.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelControles.setLayout(gbl_panelControles);
		JComboBox combo = new JComboBox();
		GridBagConstraints gbc_combo = new GridBagConstraints();
		gbc_combo.insets = new Insets(0, 0, 5, 0);
		gbc_combo.gridx = 0;
		gbc_combo.gridy = 1;
		panelControles.add(combo, gbc_combo);
		combo.setModel(new DefaultComboBoxModel(nombresLookAndFeel()));
		combo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				seleccionarLookAndFeel(combo.getSelectedItem().toString());
			}
		});
		
		comboInstancias = new JComboBox();
		GridBagConstraints gbc_comboBox = new 	GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 0;
		gbc_comboBox.gridy = 4;
		panelControles.add(comboInstancias, gbc_comboBox);
		
		botonProcesar = new JButton("Procesar");
		botonProcesar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				instanciaElegida= comboInstancias.getSelectedItem().toString();
				
			}
		});
		GridBagConstraints gbc_botonProcesar = new GridBagConstraints();
		gbc_botonProcesar.insets = new Insets(0, 0, 5, 0);
		gbc_botonProcesar.gridx = 0;
		gbc_botonProcesar.gridy = 5;
		panelControles.add(botonProcesar, gbc_botonProcesar);
		
		JButton btnReiniciar = new JButton("Reiniciar");
		btnReiniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				reinicioMapa();
			}
		});
		
		panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridheight = 4;
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 9;
		panelControles.add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[] {0, 0, 0, 8};
		gbl_panel.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblNewLabel = new JLabel("Cuantos tramos desea quitar?");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.gridheight = 2;
		gbc_lblNewLabel.gridwidth = 5;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		panel.add(lblNewLabel, gbc_lblNewLabel);
		
		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 0, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 2;
		panel.add(textField, gbc_textField);
		panel.setVisible(false);
		textField.setColumns(10);
		
		botonDividir = new JButton("Dividir");
		botonDividir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		GridBagConstraints gbc_botonDividir = new GridBagConstraints();
		gbc_botonDividir.insets = new Insets(0, 0, 0, 5);
		gbc_botonDividir.gridx = 3;
		gbc_botonDividir.gridy = 2;
		panel.add(botonDividir, gbc_botonDividir);
		GridBagConstraints gbc_btnReiniciar = new GridBagConstraints();
		gbc_btnReiniciar.gridx = 0;
		gbc_btnReiniciar.gridy = 15;
		panelControles.add(btnReiniciar, gbc_btnReiniciar);
		
	}
	
	//ESCUCHA BOTON PROCESAR
	public void escuchoProcesar(ActionListener escuchaBotonProcesar) {
		botonProcesar.addActionListener(escuchaBotonProcesar);
	}
	
	public void escuchoDividir(ActionListener cargaDividirListener) {
		botonDividir.addActionListener(cargaDividirListener);
	}
	
	public void llenoComboInstancias(ArrayList<String> insatancias) {
		for (String string : insatancias) {
			comboInstancias.addItem(string);
		}
	}
	
	public JMapViewer get_mapa() {
		return _mapa;
	}

	public void set_mapa(JMapViewer _mapa) {
		this._mapa = _mapa;
	}
	public String getInstanciaElegida() {
		return comboInstancias.getSelectedItem().toString();
	}
	public JComboBox getComboInstancias() {
		return comboInstancias;
	}

	public void setComboInstancias(JComboBox comboInstancias) {
		this.comboInstancias = comboInstancias;
	}
	
	public void inicializoMapa() {
		_mapa = new JMapViewer();
		_mapa.setDisplayPosition(new Coordinate(-34.521, -58.7008), 15);
		panelMapa.add(_mapa);
	}
	
	public void reinicioMapa() {
		_mapa.removeAllMapMarkers();
		_mapa.removeAllMapPolygons();
	}
	public JButton getBotonProcesar() {
		return botonProcesar;
	}

	public void setBotonProcesar(JButton botonProcesar) {
		this.botonProcesar = botonProcesar;
	}
	
	public void activarPanelDividir() {
		panel.setVisible(true);
	}
	
	public String getCantidadDePartes() {
		return textField.getText();
	}

	public void desactivaPanelDividir() {
		panel.setVisible(false);
		
	}
	
	public void centrarMapa(Double lat, Double lon) {
		_mapa.setDisplayPosition(new Coordinate(lat, lon), 14);
	}
	
}
