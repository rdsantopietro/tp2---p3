package entornoGrafico;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.JLabel;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.FlowLayout;

public class Resultados {

	private JFrame frame;
	private JTable table;

	/**
	 * Launch the application.
	 */
	
	
	public void llenoTabla(Object[][] valores, String [] columnas){
		
        DefaultTableModel modelo = new DefaultTableModel(valores, columnas);
        table.setModel(modelo);
        table.setBounds(12, 58, 424, 201);
		frame.getContentPane().add(table);
		SwingUtilities.updateComponentTreeUI(table);
		SwingUtilities.updateComponentTreeUI(frame);
		

	}
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Resultados window = new Resultados();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Resultados() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(650, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		JLabel lblNewLabel = new JLabel("Resultados");
		lblNewLabel.setBounds(58, 14, 117, 41);
		frame.getContentPane().add(lblNewLabel);
		lblNewLabel.setForeground(SystemColor.activeCaption);
		lblNewLabel.setBackground(SystemColor.activeCaption);
		lblNewLabel.setFont(new Font("Sawasdee", Font.BOLD, 24));
		
		table = new JTable();
		table.setBounds(400, 256, 407, -201);
		frame.getContentPane().add(table);
		lblNewLabel.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
			}
		});
		
	}
	
	public void cerrar() {
		this.frame.dispose();
	}
	public void mostrar() {
		frame.setVisible(true);
	}

}
