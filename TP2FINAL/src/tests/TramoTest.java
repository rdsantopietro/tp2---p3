package tests;
import negocio.Punto;
import negocio.Tramo;
import java.util.ArrayList;
import java.util.Collections;


import static org.junit.Assert.*;
import org.junit.Test;

class TramoTest {
	
		@Test
	public void TestArrayDesordenado() {
		ArrayList <Tramo> tramos= TramosTest1();
		assertEquals(Double.valueOf(2.0),tramos.get(1).getPeso());
		assertEquals(Double.valueOf(1.0),tramos.get(0).getPeso());		
		assertEquals(Double.valueOf(0.5),tramos.get(2).getPeso());
	}
	
	@Test
	public void ordeno() {
		ArrayList <Tramo> tramos= TramosTest1();
		assertEquals(Double.valueOf(1.0),tramos.get(0).getPeso());		
		assertEquals(Double.valueOf(2.0),tramos.get(1).getPeso());
		assertEquals(Double.valueOf(0.5),tramos.get(2).getPeso());
		Collections.sort(tramos);
		assertEquals(Double.valueOf(2.0),tramos.get(0).getPeso());		
		assertEquals(Double.valueOf(1.0),tramos.get(1).getPeso());		
		assertEquals(Double.valueOf(0.5),tramos.get(2).getPeso());
	}
	
	public ArrayList <Tramo> TramosTest1(){
		ArrayList <Tramo> tramos= new ArrayList<Tramo>();
		Punto p0= creoPunto0();
		Punto p1= creoPunto1();
		Punto p2= creoPunto2();
		Punto p3= creoPunto2();
		
		Tramo t0= new Tramo(p0,p1, 1.0);
		Tramo t1= new Tramo(p1,p2, 2.0);
		Tramo t2= new Tramo(p2,p3, 0.5);
		tramos.add(t0);
		tramos.add(t1);
		tramos.add(t2);
		
		
		return tramos;
	}
	
	
	public Punto creoPunto0() {
		Punto p1= new Punto("1321","123123",0);
		return p1;
	}
	public Punto creoPunto1() {
		Punto p1= new Punto("1321","123123",1);
		return p1;
	}
	public Punto creoPunto2() {
		Punto p1= new Punto("1321","123123",2);
		return p1;
	}

}
