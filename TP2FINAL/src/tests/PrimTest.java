package tests;

import static org.junit.Assert.*;

import org.junit.Test;


import negocio.Grafo;
import negocio.GrafoPesado;
import negocio.Prim;
import negocio.Punto;

class PrimTest {

	@Test
	public void testResultado() {
		GrafoPesado grafo= construyoGrafoPesado5Puntos();
		//SETEO PESO A MANO PARA HACER TEST
		grafo.agregarTramo(grafo.getPuntos()[1], grafo.getPuntos()[4], 5.0);
		grafo.agregarTramo(grafo.getPuntos()[4], grafo.getPuntos()[2], 2.0);
		grafo.agregarTramo(grafo.getPuntos()[4], grafo.getPuntos()[3], 3.0);
		grafo.agregarTramo(grafo.getPuntos()[3], grafo.getPuntos()[2], 1.0);
		grafo.agregarTramo(grafo.getPuntos()[3], grafo.getPuntos()[0], 8.0);
		grafo.agregarTramo(grafo.getPuntos()[4], grafo.getPuntos()[0], 7.0);
		Prim prim= new Prim(grafo);
		prim.ejecutar();
		assertFalse(prim.getGrafoSolucion().existeTramo(grafo.getPuntos()[3], grafo.getPuntos()[0]));
		assertFalse(prim.getGrafoSolucion().existeTramo(grafo.getPuntos()[2], grafo.getPuntos()[1]));
		assertTrue(prim.getGrafoSolucion().existeTramo(grafo.getPuntos()[4], grafo.getPuntos()[1]));
		assertTrue(prim.getGrafoSolucion().existeTramo(grafo.getPuntos()[2], grafo.getPuntos()[4]));
	}
	
	@Test
	public void testResultado2() {
		GrafoPesado grafo= construyoGrafoPesado5Puntos();
		//SETEO PESO A MANO PARA HACER TEST
		grafo.agregarTramo(grafo.getPuntos()[0], grafo.getPuntos()[3], 1.0);
		grafo.agregarTramo(grafo.getPuntos()[3], grafo.getPuntos()[1], 3.0);
		grafo.agregarTramo(grafo.getPuntos()[1], grafo.getPuntos()[2], 2.0);
		grafo.agregarTramo(grafo.getPuntos()[2], grafo.getPuntos()[3], 4.0);
		grafo.agregarTramo(grafo.getPuntos()[3], grafo.getPuntos()[4], 6.0);
		grafo.agregarTramo(grafo.getPuntos()[4], grafo.getPuntos()[2], 5.0);
		Prim prim= new Prim(grafo);
		prim.ejecutar();               
		
		assertFalse(prim.getGrafoSolucion().existeTramo(grafo.getPuntos()[3], grafo.getPuntos()[2]));
		assertFalse(prim.getGrafoSolucion().existeTramo(grafo.getPuntos()[3], grafo.getPuntos()[4]));
		assertTrue(prim.getGrafoSolucion().existeTramo(grafo.getPuntos()[0], grafo.getPuntos()[3]));
		assertTrue(prim.getGrafoSolucion().existeTramo(grafo.getPuntos()[3], grafo.getPuntos()[1]));
		assertTrue(prim.getGrafoSolucion().existeTramo(grafo.getPuntos()[2], grafo.getPuntos()[1]));
		assertTrue(prim.getGrafoSolucion().existeTramo(grafo.getPuntos()[2], grafo.getPuntos()[4]));
	}
	
	
	public GrafoPesado construyoGrafoPesado5Puntos() {
		GrafoPesado grafo = new GrafoPesado(5);
		Punto p1= new Punto("0.1", "0.02", 1);
		Punto p2= new Punto("0.6", "0.03", 0);
		Punto p3= new Punto("0.7", "0.03", 2);
		Punto p4= new Punto("0.8", "0.03", 3);
		Punto p5= new Punto("0.9", "0.03", 4);
		grafo.agregarPunto(p1);
		grafo.agregarPunto(p2);
		grafo.agregarPunto(p3);
		grafo.agregarPunto(p4);
		grafo.agregarPunto(p5);
		return grafo;
	}
	
	public GrafoPesado construyoGrafoPesado4Puntos() {
		GrafoPesado grafo = new GrafoPesado(4);
		Punto p1= new Punto("0.1", "0.02", 1);
		Punto p2= new Punto("0.6", "0.03", 0);
		Punto p3= new Punto("0.7", "0.03", 2);
		Punto p4= new Punto("0.8", "0.03", 3);
		grafo.agregarPunto(p1);
		grafo.agregarPunto(p2);
		grafo.agregarPunto(p3);
		grafo.agregarPunto(p4);
		
		return grafo;
	}

}
