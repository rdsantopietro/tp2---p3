package tests;

import static org.junit.Assert.*;
import org.junit.Test;
import negocio.Grafo;
import negocio.GrafoPesado;
import negocio.Prim;
import negocio.Punto;

public class PuntoTest {
	@Test
	public void agrego1Vecino() {
		Punto p0=creoPunto0();
		Punto p1=creoPunto1();
		Punto p2=creoPunto2();
		p0.agregarVecino(p1);
		p1.agregarVecino(p0);
	
		assertFalse(p2.tieneVecino(p1));
		assertFalse(p2.tieneVecino(p0));
		assertTrue(p0.tieneVecino(p1));
		assertTrue(p1.tieneVecino(p0));
	}
	@Test
	public void tamanoVecinos() {
		Punto p0=creoPunto0();
		Punto p1=creoPunto1();
		Punto p2=creoPunto2();
		p0.agregarVecino(p1);
		p1.agregarVecino(p0);
		
		assertEquals(1, p1.cantidadDeVecinos());
		assertEquals(1, p0.cantidadDeVecinos());
		assertEquals(0, p2.cantidadDeVecinos());
	}

	public Punto creoPunto0() {
		Punto p1= new Punto("1321","123123",0);
		return p1;
	}
	public Punto creoPunto1() {
		Punto p1= new Punto("1321","123123",1);
		return p1;
	}
	public Punto creoPunto2() {
		Punto p1= new Punto("1321","123123",2);
		return p1;
	}
}
