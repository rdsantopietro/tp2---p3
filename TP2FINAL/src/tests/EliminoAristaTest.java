package tests;

import negocio.*;

import static org.junit.Assert.*;

import org.junit.Test;

public class EliminoAristaTest {
	@Test(expected = IllegalArgumentException.class)
	public void primerVerticeNegativoTest() throws Exception {
		Grafo grafo = new Grafo(5);
		grafo.eliminarArista(-1, 4);
	}

	@Test(expected = IllegalArgumentException.class)
	public void primerVerticeExcedidoTest() throws Exception {
		Grafo grafo = new Grafo(5);
		grafo.eliminarArista(5, 4);
	}

	@Test(expected = IllegalArgumentException.class)
	public void segundoVerticeNegativoTest() throws Exception {
		Grafo grafo = new Grafo(5);
		grafo.eliminarArista(3, -1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void segundoVerticeExcedidoTest() throws Exception {
		Grafo grafo = new Grafo(5);
		grafo.eliminarArista(2, 5);
	}

	@Test(expected = IllegalArgumentException.class)
	public void verticesIgualesTest() throws Exception {
		Grafo grafo = new Grafo(5);
		grafo.eliminarArista(2, 2);
	}

	@Test
	public void aristaExistenteTest() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 4);
		assertTrue(grafo.existeArista(2, 4));
	}

	@Test
	public void aristaYaEliminadaTest() throws Exception {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 4);
		grafo.eliminarArista(2, 4);
		grafo.eliminarArista(2, 4);
		assertFalse(grafo.existeArista(2, 4));
	}

	@Test
	public void aristaInvertidaTest() throws Exception {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3);
		grafo.eliminarArista(2, 3);
		assertFalse(grafo.existeArista(3, 2));
	}

	@Test
	public void aristaInexistenteTest() throws Exception {
		Grafo grafo = new Grafo(5);
		grafo.eliminarArista(2, 0);
		assertFalse(grafo.existeArista(2, 0));
	}
}
