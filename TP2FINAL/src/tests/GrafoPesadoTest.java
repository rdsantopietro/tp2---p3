package tests;



import static org.junit.Assert.*;
import org.junit.Test;

import negocio.*;


public class GrafoPesadoTest {
	@Test(expected = IllegalArgumentException.class)
	public void agregoPuntoNegativo() {
		GrafoPesado grafo = new GrafoPesado(5);
		Punto nuevoPunto = new Punto("0.1", "0.02", -5);
		grafo.agregarPunto(nuevoPunto);
		
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregoPuntoMayorTamano() {
		GrafoPesado grafo = new GrafoPesado(5);
		Punto nuevoPunto = new Punto("0.1", "0.02", 10);
		grafo.agregarPunto(nuevoPunto);
		
	}

	@Test(expected = Exception.class)
	public void pesoNegativo() throws Exception {
		GrafoPesado grafo = new GrafoPesado(5);
		Punto p1= new Punto("0.1", "0.02", 1);
		Punto p2= new Punto("0.2", "0.03", 0);
		grafo.agregarTramo(p1, p2, -2.05);
	}
	@Test
	public void agregarPesoCero()  {
		GrafoPesado grafo = new GrafoPesado(5);
		Punto p1= new Punto("0.1", "0.02", 1);
		Punto p2= new Punto("0.2", "0.03", 0);
		grafo.agregarTramo(p1, p2, 0.0);
		assertTrue(grafo.existeTramo(p1, p2));
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void puntosIguales()  {
		GrafoPesado grafo = new GrafoPesado(5);
		Punto p1= new Punto("0.1", "0.02", 1);
		grafo.agregarTramo(p1, p1, 1.0);
	}

	@Test
	public void tramoExistente() {
		GrafoPesado grafo = new GrafoPesado(5);
		Punto p1= new Punto("0.1", "0.02", 1);
		Punto p2= new Punto("0.6", "0.03", 0);
		grafo.agregarTramo(p1, p2, 1.0);
		assertTrue(grafo.existeTramo(p1, p2));
	}
	@Test
	public void tramoRepetidoExistente() {
		GrafoPesado grafo = new GrafoPesado(5);
		Punto p1= new Punto("0.1", "0.02", 1);
		Punto p2= new Punto("0.6", "0.03", 0);
		grafo.agregarTramo(p1, p2, 1.0);
		grafo.agregarTramo(p1, p2, 1.0);
		assertTrue(grafo.existeTramo(p1, p2));
	}
	
	@Test
	public void vecinoDeLosPuntos() {
		GrafoPesado grafo = new GrafoPesado(5);
		Punto p1= new Punto("0.1", "0.02", 1);
		Punto p2= new Punto("0.6", "0.03", 0);
		Punto p3= new Punto("0.6", "0.03", 2);
		grafo.agregarTramo(p1, p2, 1.0);
		
		assertTrue(p1.tieneVecino(p2));
		assertTrue(p2.tieneVecino(p1));
		assertFalse(p2.tieneVecino(p3));
		assertFalse(p1.tieneVecino(p3));
	}
	
	@Test
	public void cantidadDeVecinos() {
		GrafoPesado grafo = new GrafoPesado(5);
		Punto p1= new Punto("0.1", "0.02", 1);
		Punto p2= new Punto("0.6", "0.03", 0);
		Punto p3= new Punto("0.6", "0.03", 2);
		Punto p4= new Punto("0.6", "0.03", 3);
		
		grafo.agregarTramo(p1, p2, 1.0);
		grafo.agregarTramo(p1, p4, 1.0);
		
		assertEquals(2, p1.cantidadDeVecinos());;
		assertEquals(1, p4.cantidadDeVecinos());;
		assertEquals(1, p2.cantidadDeVecinos());;
		assertEquals(0, p3.cantidadDeVecinos());;
		
		
	}
	
	@Test
	public void tramoInexistente() {
		GrafoPesado grafo = new GrafoPesado(5);
		Punto p1= new Punto("0.1", "0.02", 1);
		Punto p2= new Punto("0.6", "0.03", 0);
		assertFalse(grafo.existeTramo(p1, p2));
	}

	@Test
	public void aristaRepetidaTest() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 4);
		grafo.agregarArista(2, 4);
		assertTrue(grafo.existeArista(2, 4));
	}

	@Test
	public void tramoInvertido() {
		GrafoPesado grafo = new GrafoPesado(5);
		Punto p1= new Punto("0.1", "0.02", 1);
		Punto p2= new Punto("0.6", "0.03", 0);
		grafo.agregarTramo(p1, p2, 1.0);
		assertTrue(grafo.existeTramo(p2,p1));
	}
}
